# Sublime Text 3 packages and settings.

## Installation.

1. [Install Sublime Text 3](http://docs.sublimetext.info/en/latest/index.html).
2. [Install Package Control](https://packagecontrol.io/installation).
3. Close Sublime Text 3.
4. Fetch this repo in your `~/.config/sublime-text-3/Packages/User` folder, overriding your local settings.

        git init
        git remote add origin https://bitbucket.org/manueljaguilera/st3-setup/
        git fetch origin
        git reset --hard origin/master

5. Open Sublime Text 3.